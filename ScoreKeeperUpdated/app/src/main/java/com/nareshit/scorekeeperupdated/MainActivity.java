package com.nareshit.scorekeeperupdated;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.nareshit.scorekeeperupdated.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityMainBinding binding;
    private MainViewModel mvm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*This line is not needed as we have to initialize this binding object -->
        setContentView(R.layout.activity_main);*/
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        // if there is an existing viewmodel for this activity - we need to bind to it
        // otherwise create a new View Model.
        mvm = new ViewModelProvider(this).get(MainViewModel.class);
        binding.score.setText(String.valueOf(mvm.getCount()));
        Log.i("MainActivity", "View MOdel is initialized");
        // We are avoiding the findViewById() method calls.
        // Direct accessing of Views on the xml file is now enabled.
        binding.plusBtn.setOnClickListener(this);
        binding.minusBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == binding.plusBtn.getId()){
            mvm.increment();
        }else{
            mvm.decrement();
        }
        binding.score.setText(String.valueOf(mvm.getCount()));
    }
}