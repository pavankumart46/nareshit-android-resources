package com.nareshit.studentdetailssql;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText studentName, studentAge;
    private TextView result;
    DatabaseHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        dbHelper = new DatabaseHelper(this);
    }

    public void saveData(View view) {
        // Get the Data from the form
        String n = studentName.getText().toString();
        int a = Integer.parseInt(studentAge.getText().toString());

        // Map it with the help of ContentValues
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COL1,n);
        cv.put(DatabaseHelper.COL2,a);
        // Call the insertData method on DAtabaseHelper class
        dbHelper.insertData(cv);

        Toast.makeText(this, "DATA INSERTION SUCCESSFUL", Toast.LENGTH_SHORT).show();

        studentAge.getText().clear();
        studentName.getText().clear();
    }


    public void loadData(View view) {
        // First Query the table and get the data in the form of a Cursor Object
        Cursor cursor = dbHelper.getValues();
        // Display the data
        cursor.moveToFirst();
        StringBuilder sb = new StringBuilder();
        do{
            Toast.makeText(this, cursor.getString(1), Toast.LENGTH_SHORT).show();
            sb.append("ID: "+cursor.getInt(0)+" ");
            sb.append("Student Name: "+cursor.getString(1)+" ");
            sb.append("Student Age: "+cursor.getInt(2)+"\n\n");
        }while(cursor.moveToNext());
        cursor.close();
        result.setText(sb.toString());
    }

    private void initViews() {
        studentName = findViewById(R.id.student_name);
        studentAge = findViewById(R.id.student_age);
        result = findViewById(R.id.result);
    }
}