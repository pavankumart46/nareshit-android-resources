package com.nareshit.googlebooks;

import com.nareshit.googlebooks.models.SourceData;

import javax.xml.transform.Source;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

//This Interface will have all the request methods that we
// want
public interface BookService {

    @GET("volumes?")
    Call<SourceData> getBookData(@Query("q") String query);

    @GET("mylibrary/bookshelves?q=")
    Call<SourceData> getPopularBooks();
}
