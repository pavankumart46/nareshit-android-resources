package com.nareshit.mycontacts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listview);
        //TODO 1: Get the Users Permission to read CONTACTS.
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED){
            // REQUEST the PERMISSION
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},123);
        }else{
            // if the permission is already granted
            readContacts();
        }
    }

    // we will read contacts
    private void readContacts() {
        // TODO 2: get the content Resolver
        ContentResolver contentResolver = getContentResolver();
        // TODO 3: query the content provider with the help of Content Resolver using the content URI
        Cursor cursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
         String[] data = {
                 ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                 ContactsContract.CommonDataKinds.Phone.NUMBER,
                 ContactsContract.CommonDataKinds.Phone._ID};
         int[] to = {android.R.id.text1, android.R.id.text2};

         // TODO 4: Use SimpleCursorAdapter that extracts data from the cursor and populates it on the listview
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2, cursor,data,to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        listView.setAdapter(adapter);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 123){
            if(permissions[0] == Manifest.permission.READ_CONTACTS){
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    readContacts(); // Because the permission is ganted. So we can read the contacts.
                }
            }
        }
    }
}