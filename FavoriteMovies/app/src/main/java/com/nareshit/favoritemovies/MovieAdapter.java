package com.nareshit.favoritemovies;

import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

// TODO: Step 4 - Add an Adapter along with ViewHolder
// Adapter will connect the data to the recyclerview with the help of ViewHolder subclass
public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder>
{
    private Context context;
    private List<Integer> movie_posters;
    private List<String> movie_names;
    private List<String> movie_actors;

    public MovieAdapter(Context context, List<Integer> movie_posters, List<String> movie_names, List<String> movie_actors) {
        this.context = context;
        this.movie_posters = movie_posters;
        this.movie_names = movie_names;
        this.movie_actors = movie_actors;
    }

    // This method will attach the layout design on each and every entry in the recyclerview
    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.one_item_design,parent, false);
        return new MovieViewHolder(v);
    }

    // This method will actually populate the data on the recyclerview.
    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        holder.mp.setImageResource(movie_posters.get(position));
        holder.mn.setText(movie_names.get(position));
        holder.ma.setText(movie_actors.get(position));
    }

    // This tells how many items are going to be displayed on top of the recyclerview
    @Override
    public int getItemCount() {
        return movie_posters.size();
    }

    // TODO: Sub Step 4 - Add a ViewHolder
    // This is for ViewHolder - and It's responsibility is to grab the UI components on
    // One_item_design.xml and connect the views
    class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView mn, ma;
        public ImageView mp;

        public MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            mn = itemView.findViewById(R.id.movie_name_textview);
            ma = itemView.findViewById(R.id.movie_actors_textview);
            mp = itemView.findViewById(R.id.movie_poster_imageview);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            // Get the position where the click is happening using getAdapterPosition() method
            int position = getAdapterPosition();
            // Get the data based on  the current position of the item
            int image_id = movie_posters.get(position);
            String movie_name = movie_names.get(position);
            String movie_actor = movie_actors.get(position);
            //Create an Intent
            Intent i = new Intent(context,MovieDetailsActivity.class);
            i.putExtra(MovieDetailsActivity.IMAGE_CONSTANT,image_id);
            i.putExtra(MovieDetailsActivity.NAME_CONSTANT,movie_name);
            i.putExtra(MovieDetailsActivity.ACTOR_CONSTANT, movie_actor);
            context.startActivity(i);
        }
    }
}
