package com.nareshtechnologies.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.net.wifi.hotspot2.pps.HomeSp;
import android.os.Build;
import android.os.Bundle;
import android.telephony.CellInfo;
import android.telephony.CellSignalStrength;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TelephonyManager tm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
        && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)!= PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_SMS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION}, 123);
            return;
        }
        String deviceid = tm.getLine1Number();
        List<CellInfo> cellInfoList = tm.getAllCellInfo();
        for(int i=0; i< cellInfoList.size(); i++){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                int a = cellInfoList.get(i).getCellConnectionStatus();
                switch (a){
                    case CellInfo.CONNECTION_NONE:
                        Toast.makeText(this, "NO CONNECTION", Toast.LENGTH_SHORT).show();
                        break;
                    case CellInfo.CONNECTION_PRIMARY_SERVING:
                        Toast.makeText(this, "Primary Serving", Toast.LENGTH_SHORT).show();
                        break;
                    case CellInfo.CONNECTION_SECONDARY_SERVING:
                        Toast.makeText(this, "Secondary Serving", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }
        Toast.makeText(this, deviceid, Toast.LENGTH_SHORT).show();
    }
}