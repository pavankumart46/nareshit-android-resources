package com.nareshit.hydrateyourself;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // Here We will Handle the Alarm.
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "ALARM");
        builder.setContentTitle("DRINK WATER");
        builder.setContentText("DRINKING WATER IS HEALTHy");
        builder.setSmallIcon(R.drawable.ic_launcher_foreground);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel("ALARM", "HYDRATION REMAINDER", NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription("This channel reminds you of drinking water");
            manager.createNotificationChannel(channel);
        }

        Intent i = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,123,i,PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(pendingIntent);

        manager.notify(123,builder.build());

    }
}
