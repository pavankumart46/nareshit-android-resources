package com.nareshit.pickersinandroid;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;
import java.util.Date;

public class DateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    // Is resposible for showing the picker dialog to the user
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Calendar c = Calendar.getInstance(); // This will get the calendar from java.util package.
        int year = c.get(Calendar.YEAR); // gets the current year.
        int month = c.get(Calendar.MONTH); // gets the current month.
        int dayofMonth = c.get(Calendar.DAY_OF_MONTH); // gets the current date.
        DatePickerDialog picker = new DatePickerDialog(getActivity(),this,year, month, dayofMonth);
        picker.getDatePicker().setMinDate(System.currentTimeMillis());
        picker.getDatePicker().setMaxDate(System.currentTimeMillis()+(1000*60*60*24*7));
        return picker;
    }

    // It is a callback called when the user selects a date.
    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayofMonth) {
        MainActivity activity = (MainActivity) getActivity();
        activity.dateDisplay(year,month+1,dayofMonth);
    }
}
