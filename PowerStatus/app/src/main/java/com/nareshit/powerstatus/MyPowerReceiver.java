package com.nareshit.powerstatus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.Toast;

// This class is used to perform some action
// when there is a broadcast
public class MyPowerReceiver extends BroadcastReceiver {

    private ImageView img;
    boolean status = false;

    public MyPowerReceiver(ImageView img) {
        this.img = img;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()){
            case Intent.ACTION_AIRPLANE_MODE_CHANGED:
                if(status){
                    img.setImageResource(R.drawable.airplane_mode_on);
                    status = false;
                }else{
                    img.setImageResource(R.drawable.airplane_mode_off);
                    status = true;
                }
                break;
            case Intent.ACTION_POWER_CONNECTED:
                img.setImageResource(R.drawable.battery_charging);
                Toast.makeText(context, "CHARGER CONNECTED", Toast.LENGTH_SHORT).show();
                break;
            case Intent.ACTION_POWER_DISCONNECTED:
                img.setImageResource(R.drawable.battery_discharging);
                Toast.makeText(context, "CHARGER DISCONNECTED", Toast.LENGTH_SHORT).show();
                break;

            case MainActivity.CUSTOM_BROADCAST:
                Toast.makeText(context, "CUSTOM BROADCAST RECEIVED", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
