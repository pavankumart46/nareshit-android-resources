package com.nareshit.permissionsinandroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Switch camera, location;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        camera = findViewById(R.id.camera_permission);
        location = findViewById(R.id.accesslocation);
        // TODO 2: Check if the permission is granted using contextcompat.checkselfpermission() Method
        if((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED)){
            Toast.makeText(this, "PERMISSION IS GRANTED", Toast.LENGTH_SHORT).show();
        }else{
            /*Toast.makeText(this, "PERMISSION NOT GRANTED", Toast.LENGTH_SHORT).show();*/
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.ACCESS_FINE_LOCATION},123);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, 
                                           @NonNull String[] permissions, 
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 123){
            for(int i=0; i<permissions.length; i++){
                switch (permissions[i]){
                    case Manifest.permission.CAMERA:
                        if(grantResults.length>0 && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            camera.setChecked(true);
                            Toast.makeText(this, "THANK YOU", Toast.LENGTH_SHORT).show();
                        }else {
                            camera.setChecked(false);
                            Toast.makeText(this, "THIS IS IMP PERMISSION, Some Features May not work!", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case Manifest.permission.ACCESS_FINE_LOCATION:
                        if(grantResults.length>0 && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            location.setChecked(true);
                            Toast.makeText(this, "THANK YOU", Toast.LENGTH_SHORT).show();
                        }else {
                            location.setChecked(false);
                            Toast.makeText(this, "THIS IS IMP PERMISSION, Some Features May not work!", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }

            }
        }
    }

    // TODO 3: if not Granted
    // TODO 3.1: requestPermissions()
    // TODO 3.2: if Granted- Continue with the workflow
}