package com.nareshit.musicplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import java.net.MalformedURLException;

public class MainActivity extends AppCompatActivity {
    private Intent musicServiceIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        musicServiceIntent = new Intent(this,MusicService.class);
    }


    public void startMusic(View view) {
        startService(musicServiceIntent);
    }

    public void stopMusic(View view) {
        stopService(musicServiceIntent);
    }
}