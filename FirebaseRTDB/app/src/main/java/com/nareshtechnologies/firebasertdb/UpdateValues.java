package com.nareshtechnologies.firebasertdb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class UpdateValues extends AppCompatActivity {

    TextView textView;
    EditText n,a;
    DatabaseReference dbr;
    String key;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_values);
        dbr = FirebaseDatabase.getInstance().getReference();
        textView = findViewById(R.id.textView);
        n = findViewById(R.id.student_name2);
        a = findViewById(R.id.student_age2);
        Intent i = getIntent();
        key = i.getStringExtra("KEY");
        textView.setText(key);
        n.setText(i.getStringExtra("NAME"));
        a.setText(i.getStringExtra("AGE"));

    }

    public void updateData(View view)
    {
        String name = n.getText().toString();
        String age = a.getText().toString();
        HashMap<String, Object> h = new HashMap<>();
        h.put("name",name);
        h.put("age",age);
        dbr.child(key).updateChildren(h).addOnSuccessListener(su->{
            Toast.makeText(this, "UPDATED SUCCESSFULY", Toast.LENGTH_SHORT).show();
        });
    }
}