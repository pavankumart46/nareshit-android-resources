package com.nareshtechnologies.roomdatabaseexample;

// TODO 2: Create an Enity class that represents a table in ROOM Database
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/** The following class is called an "Entity class"
 * Each Entity Class in room represents a table
 * Entity Class name -> becomes table name
 * Entity Class Variables -> becomes coloumn names of the table
 * Represent an entity class with @Entity Annotation*/

@Entity(tableName = "student")
public class Student {
    @PrimaryKey(autoGenerate = true)
    int stu_id;
    String stu_name;
    int stu_age;

    public Student(String stu_name, int stu_age) {
        this.stu_name = stu_name;
        this.stu_age = stu_age;
    }

    public int getStu_id() {
        return stu_id;
    }

    public void setStu_id(int stu_id) {
        this.stu_id = stu_id;
    }

    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }

    public int getStu_age() {
        return stu_age;
    }

    public void setStu_age(int stu_age) {
        this.stu_age = stu_age;
    }
}
