package com.nareshit.jobscheduler;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.widget.Toast;

// TODO 1: Create a MyService class and extend this to JobService class
//  - override onStartJob() and onStopJob()
public class MyService extends JobService {

    // TODO 2: Write the Code related to running the task inside onStartJob()
    //  - if you want to reschedule the task(if it is stopped by the system)
    //  return true otherwise false from onStopJob()

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        Toast.makeText(this, "JOB IS SCHEDULED AND FINISHED", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }
}
