package com.nareshit.pickersinandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView dateText;
    private TextView timeText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dateText = findViewById(R.id.date_display);
        timeText = findViewById(R.id.time_display);
    }

    // have to show date picker dialog to the user
    public void pickDate(View view) {
        DateFragment fragment = new DateFragment();
        fragment.show(getSupportFragmentManager(),"Date Picker");
    }

    // Method to show the date
    public void dateDisplay(int year, int month, int dayOfMonth){
        dateText.setText(dayOfMonth+"-"+month+"-"+year);
    }

    public void pickTime(View view) {
        TimeFragment fragment = new TimeFragment();
        fragment.show(getSupportFragmentManager(),"Time Picker");
    }

    // Method to show the date
    public void timeDisplay(int hour, int min, String ampm){
        timeText.setText(hour+":"+min+" "+ampm);
    }
}
