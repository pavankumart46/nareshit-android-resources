package com.nareshit.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "Pavan.db";
    public static int VERSION = 1;

    public static String TABLE_NAME = "students";
    public static String COL0 = "stu_id";
    public static String COL1 = "name";
    public static String COL2 = "age";
    public static String COL3 = "phone";

    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    //Creating the tables
    @Override
    public void onCreate(SQLiteDatabase db) {
            String createTable = "CREATE TABLE "+TABLE_NAME+" ("+COL0+" integer primary key " +
                    "autoincrement,"+COL1+" text,"+COL2+" integer,"+COL3+" integer);";
            db.execSQL(createTable);
    }
    // Inserting into tables
    public void insertData(String name, int age, int phone){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL1,name);
        cv.put(COL2,age);
        cv.put(COL3,phone);

        db.insert(TABLE_NAME,null,cv);

        db.close();
    }

    public Cursor readData(){
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("SELECT * FROM "+TABLE_NAME,null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop table "+TABLE_NAME);
        onCreate(db);
    }
}
