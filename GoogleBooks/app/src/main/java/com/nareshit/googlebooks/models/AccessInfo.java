
package com.nareshit.googlebooks.models;

import java.util.HashMap;
import java.util.Map;

public class AccessInfo {

    private String country;
    private String viewability;
    private boolean embeddable;
    private boolean publicDomain;
    private String textToSpeechPermission;
    private Epub epub;
    private Pdf pdf;
    private String webReaderLink;
    private String accessViewStatus;
    private boolean quoteSharingAllowed;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public AccessInfo() {
    }

    /**
     * 
     * @param accessViewStatus
     * @param country
     * @param viewability
     * @param pdf
     * @param webReaderLink
     * @param epub
     * @param publicDomain
     * @param quoteSharingAllowed
     * @param embeddable
     * @param textToSpeechPermission
     */
    public AccessInfo(String country, String viewability, boolean embeddable, boolean publicDomain, String textToSpeechPermission, Epub epub, Pdf pdf, String webReaderLink, String accessViewStatus, boolean quoteSharingAllowed) {
        super();
        this.country = country;
        this.viewability = viewability;
        this.embeddable = embeddable;
        this.publicDomain = publicDomain;
        this.textToSpeechPermission = textToSpeechPermission;
        this.epub = epub;
        this.pdf = pdf;
        this.webReaderLink = webReaderLink;
        this.accessViewStatus = accessViewStatus;
        this.quoteSharingAllowed = quoteSharingAllowed;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getViewability() {
        return viewability;
    }

    public void setViewability(String viewability) {
        this.viewability = viewability;
    }

    public boolean isEmbeddable() {
        return embeddable;
    }

    public void setEmbeddable(boolean embeddable) {
        this.embeddable = embeddable;
    }

    public boolean isPublicDomain() {
        return publicDomain;
    }

    public void setPublicDomain(boolean publicDomain) {
        this.publicDomain = publicDomain;
    }

    public String getTextToSpeechPermission() {
        return textToSpeechPermission;
    }

    public void setTextToSpeechPermission(String textToSpeechPermission) {
        this.textToSpeechPermission = textToSpeechPermission;
    }

    public Epub getEpub() {
        return epub;
    }

    public void setEpub(Epub epub) {
        this.epub = epub;
    }

    public Pdf getPdf() {
        return pdf;
    }

    public void setPdf(Pdf pdf) {
        this.pdf = pdf;
    }

    public String getWebReaderLink() {
        return webReaderLink;
    }

    public void setWebReaderLink(String webReaderLink) {
        this.webReaderLink = webReaderLink;
    }

    public String getAccessViewStatus() {
        return accessViewStatus;
    }

    public void setAccessViewStatus(String accessViewStatus) {
        this.accessViewStatus = accessViewStatus;
    }

    public boolean isQuoteSharingAllowed() {
        return quoteSharingAllowed;
    }

    public void setQuoteSharingAllowed(boolean quoteSharingAllowed) {
        this.quoteSharingAllowed = quoteSharingAllowed;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(AccessInfo.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("country");
        sb.append('=');
        sb.append(((this.country == null)?"<null>":this.country));
        sb.append(',');
        sb.append("viewability");
        sb.append('=');
        sb.append(((this.viewability == null)?"<null>":this.viewability));
        sb.append(',');
        sb.append("embeddable");
        sb.append('=');
        sb.append(this.embeddable);
        sb.append(',');
        sb.append("publicDomain");
        sb.append('=');
        sb.append(this.publicDomain);
        sb.append(',');
        sb.append("textToSpeechPermission");
        sb.append('=');
        sb.append(((this.textToSpeechPermission == null)?"<null>":this.textToSpeechPermission));
        sb.append(',');
        sb.append("epub");
        sb.append('=');
        sb.append(((this.epub == null)?"<null>":this.epub));
        sb.append(',');
        sb.append("pdf");
        sb.append('=');
        sb.append(((this.pdf == null)?"<null>":this.pdf));
        sb.append(',');
        sb.append("webReaderLink");
        sb.append('=');
        sb.append(((this.webReaderLink == null)?"<null>":this.webReaderLink));
        sb.append(',');
        sb.append("accessViewStatus");
        sb.append('=');
        sb.append(((this.accessViewStatus == null)?"<null>":this.accessViewStatus));
        sb.append(',');
        sb.append("quoteSharingAllowed");
        sb.append('=');
        sb.append(this.quoteSharingAllowed);
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}
