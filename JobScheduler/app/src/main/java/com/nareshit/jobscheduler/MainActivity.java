package com.nareshit.jobscheduler;

import androidx.appcompat.app.AppCompatActivity;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    // TODO 4: on MainActivity.java create JobInfo.Builder object and set the conditions
    private JobInfo jobInfo;
    // JobId is constant integer
    private static final int JOB_ID = 123;

    // TODO 5: Create a JobScheduler using getSystemService(JOB_SCHEDULER_SERVICE)
    JobScheduler scheduler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // intialize the scheduler
        scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);

        ComponentName cname = new ComponentName(getPackageName(),MyService.class.getName());
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID,cname);
        // Write conditions for job execution on top of the builder
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);

        // intialize JobInfo from builder object
        jobInfo = builder.build();
    }

    public void scheduleJob(View view) {
    // TODO 6: schedule the job using schedule() on top of the jobScheduler object
        scheduler.schedule(jobInfo);
    }

    public void cancelJob(View view) {
        scheduler.cancel(JOB_ID);
    }
}