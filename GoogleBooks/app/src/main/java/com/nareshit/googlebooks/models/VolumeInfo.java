
package com.nareshit.googlebooks.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class VolumeInfo {

    private String title;
    private List<String> authors = null;
    private String publisher;
    private String publishedDate;
    private String description;
    private List<IndustryIdentifier> industryIdentifiers = null;
    private ReadingModes readingModes;
    private long pageCount;
    private String printType;
    private List<String> categories = null;
    private String maturityRating;
    private boolean allowAnonLogging;
    private String contentVersion;
    private PanelizationSummary panelizationSummary;
    private ImageLinks imageLinks;
    private String language;
    private String previewLink;
    private String infoLink;
    private String canonicalVolumeLink;
    private String subtitle;
    private double averageRating;
    private double ratingsCount;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public VolumeInfo() {
    }

    /**
     * 
     * @param industryIdentifiers
     * @param pageCount
     * @param printType
     * @param readingModes
     * @param previewLink
     * @param canonicalVolumeLink
     * @param description
     * @param language
     * @param title
     * @param imageLinks
     * @param panelizationSummary
     * @param subtitle
     * @param averageRating
     * @param publisher
     * @param ratingsCount
     * @param publishedDate
     * @param categories
     * @param maturityRating
     * @param allowAnonLogging
     * @param contentVersion
     * @param authors
     * @param infoLink
     */
    public VolumeInfo(String title, List<String> authors, String publisher, String publishedDate, String description, List<IndustryIdentifier> industryIdentifiers, ReadingModes readingModes, long pageCount, String printType, List<String> categories, String maturityRating, boolean allowAnonLogging, String contentVersion, PanelizationSummary panelizationSummary, ImageLinks imageLinks, String language, String previewLink, String infoLink, String canonicalVolumeLink, String subtitle, long averageRating, long ratingsCount) {
        super();
        this.title = title;
        this.authors = authors;
        this.publisher = publisher;
        this.publishedDate = publishedDate;
        this.description = description;
        this.industryIdentifiers = industryIdentifiers;
        this.readingModes = readingModes;
        this.pageCount = pageCount;
        this.printType = printType;
        this.categories = categories;
        this.maturityRating = maturityRating;
        this.allowAnonLogging = allowAnonLogging;
        this.contentVersion = contentVersion;
        this.panelizationSummary = panelizationSummary;
        this.imageLinks = imageLinks;
        this.language = language;
        this.previewLink = previewLink;
        this.infoLink = infoLink;
        this.canonicalVolumeLink = canonicalVolumeLink;
        this.subtitle = subtitle;
        this.averageRating = averageRating;
        this.ratingsCount = ratingsCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<IndustryIdentifier> getIndustryIdentifiers() {
        return industryIdentifiers;
    }

    public void setIndustryIdentifiers(List<IndustryIdentifier> industryIdentifiers) {
        this.industryIdentifiers = industryIdentifiers;
    }

    public ReadingModes getReadingModes() {
        return readingModes;
    }

    public void setReadingModes(ReadingModes readingModes) {
        this.readingModes = readingModes;
    }

    public long getPageCount() {
        return pageCount;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }

    public String getPrintType() {
        return printType;
    }

    public void setPrintType(String printType) {
        this.printType = printType;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getMaturityRating() {
        return maturityRating;
    }

    public void setMaturityRating(String maturityRating) {
        this.maturityRating = maturityRating;
    }

    public boolean isAllowAnonLogging() {
        return allowAnonLogging;
    }

    public void setAllowAnonLogging(boolean allowAnonLogging) {
        this.allowAnonLogging = allowAnonLogging;
    }

    public String getContentVersion() {
        return contentVersion;
    }

    public void setContentVersion(String contentVersion) {
        this.contentVersion = contentVersion;
    }

    public PanelizationSummary getPanelizationSummary() {
        return panelizationSummary;
    }

    public void setPanelizationSummary(PanelizationSummary panelizationSummary) {
        this.panelizationSummary = panelizationSummary;
    }

    public ImageLinks getImageLinks() {
        return imageLinks;
    }

    public void setImageLinks(ImageLinks imageLinks) {
        this.imageLinks = imageLinks;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPreviewLink() {
        return previewLink;
    }

    public void setPreviewLink(String previewLink) {
        this.previewLink = previewLink;
    }

    public String getInfoLink() {
        return infoLink;
    }

    public void setInfoLink(String infoLink) {
        this.infoLink = infoLink;
    }

    public String getCanonicalVolumeLink() {
        return canonicalVolumeLink;
    }

    public void setCanonicalVolumeLink(String canonicalVolumeLink) {
        this.canonicalVolumeLink = canonicalVolumeLink;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    public double getRatingsCount() {
        return ratingsCount;
    }

    public void setRatingsCount(double ratingsCount) {
        this.ratingsCount = ratingsCount;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(VolumeInfo.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("title");
        sb.append('=');
        sb.append(((this.title == null)?"<null>":this.title));
        sb.append(',');
        sb.append("authors");
        sb.append('=');
        sb.append(((this.authors == null)?"<null>":this.authors));
        sb.append(',');
        sb.append("publisher");
        sb.append('=');
        sb.append(((this.publisher == null)?"<null>":this.publisher));
        sb.append(',');
        sb.append("publishedDate");
        sb.append('=');
        sb.append(((this.publishedDate == null)?"<null>":this.publishedDate));
        sb.append(',');
        sb.append("description");
        sb.append('=');
        sb.append(((this.description == null)?"<null>":this.description));
        sb.append(',');
        sb.append("industryIdentifiers");
        sb.append('=');
        sb.append(((this.industryIdentifiers == null)?"<null>":this.industryIdentifiers));
        sb.append(',');
        sb.append("readingModes");
        sb.append('=');
        sb.append(((this.readingModes == null)?"<null>":this.readingModes));
        sb.append(',');
        sb.append("pageCount");
        sb.append('=');
        sb.append(this.pageCount);
        sb.append(',');
        sb.append("printType");
        sb.append('=');
        sb.append(((this.printType == null)?"<null>":this.printType));
        sb.append(',');
        sb.append("categories");
        sb.append('=');
        sb.append(((this.categories == null)?"<null>":this.categories));
        sb.append(',');
        sb.append("maturityRating");
        sb.append('=');
        sb.append(((this.maturityRating == null)?"<null>":this.maturityRating));
        sb.append(',');
        sb.append("allowAnonLogging");
        sb.append('=');
        sb.append(this.allowAnonLogging);
        sb.append(',');
        sb.append("contentVersion");
        sb.append('=');
        sb.append(((this.contentVersion == null)?"<null>":this.contentVersion));
        sb.append(',');
        sb.append("panelizationSummary");
        sb.append('=');
        sb.append(((this.panelizationSummary == null)?"<null>":this.panelizationSummary));
        sb.append(',');
        sb.append("imageLinks");
        sb.append('=');
        sb.append(((this.imageLinks == null)?"<null>":this.imageLinks));
        sb.append(',');
        sb.append("language");
        sb.append('=');
        sb.append(((this.language == null)?"<null>":this.language));
        sb.append(',');
        sb.append("previewLink");
        sb.append('=');
        sb.append(((this.previewLink == null)?"<null>":this.previewLink));
        sb.append(',');
        sb.append("infoLink");
        sb.append('=');
        sb.append(((this.infoLink == null)?"<null>":this.infoLink));
        sb.append(',');
        sb.append("canonicalVolumeLink");
        sb.append('=');
        sb.append(((this.canonicalVolumeLink == null)?"<null>":this.canonicalVolumeLink));
        sb.append(',');
        sb.append("subtitle");
        sb.append('=');
        sb.append(((this.subtitle == null)?"<null>":this.subtitle));
        sb.append(',');
        sb.append("averageRating");
        sb.append('=');
        sb.append(this.averageRating);
        sb.append(',');
        sb.append("ratingsCount");
        sb.append('=');
        sb.append(this.ratingsCount);
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}
