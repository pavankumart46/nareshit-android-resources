package com.nareshtechnologies.firebasertdb;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.RVViewholder> {
    Context context;
    ArrayList<Student> studentList = new ArrayList<>();
    ArrayList<String> keys = new ArrayList<>();
    DatabaseReference reference;
    public RVAdapter(Context context, DatabaseReference databaseReference) {
        this.context = context;
        this.reference = databaseReference;
    }

    public void setItems(List<Student> student, List<String> k){
        studentList.addAll(student);
        keys.addAll(k);
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public RVViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.one_row_design,parent,false);
        return new RVViewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RVViewholder holder, int position) {
        holder.nameTv.setText(studentList.get(position).getName());
        holder.ageTv.setText(studentList.get(position).getAge());
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public class RVViewholder extends RecyclerView.ViewHolder {
        TextView nameTv, ageTv;
        Button delete, update;
        public RVViewholder(@NonNull View itemView) {
            super(itemView);
            nameTv = itemView.findViewById(R.id.name_tv);
            ageTv = itemView.findViewById(R.id.age_tv);
            delete = itemView.findViewById(R.id.button3);
            delete.setOnClickListener(v->{
                int position = getAdapterPosition();
                String key = keys.get(position);
                reference.child(key).removeValue().addOnSuccessListener(suc->{
                    Toast.makeText(context, "DELETED!", Toast.LENGTH_SHORT).show();
                });
            });
            update = itemView.findViewById(R.id.button4);
            update.setOnClickListener(v->{
                int pos = getAdapterPosition();
                Student s = studentList.get(pos);
                Intent i = new Intent(context, UpdateValues.class);
                i.putExtra("KEY", keys.get(pos));
                i.putExtra("NAME", s.getName());
                i.putExtra("AGE", s.getAge());
                context.startActivity(i);
            });
        }
    }
}
