package com.nareshit.tabnavigation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

// ViewPager Adapter to Populate data on ViewPager
public class ViewPagerAdapter extends FragmentPagerAdapter
{
    public ViewPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new RedFragment();
            case 1:
                return new BlueFragment();
            case 2:
                return new GreenFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "RED";
            case 1:
                return "BLUE";
            case 2:
                return "GREEN";
        }
        return super.getPageTitle(position);
    }
}
