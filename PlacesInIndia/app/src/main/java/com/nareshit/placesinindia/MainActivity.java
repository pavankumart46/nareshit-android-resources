package com.nareshit.placesinindia;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView mPlacesListView;
    private List<String[]> places;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPlacesListView = findViewById(R.id.places_list);
        intializePlaces();
        /*ArrayAdapter<String[]> placesAdapter = new ArrayAdapter<String[]>(this,
                android.R.layout.simple_list_item_2,
                android.R.id.text1,
                places){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                if(convertView==null){
                    convertView = LayoutInflater.from(MainActivity.this).inflate(android.R.layout.simple_list_item_2,parent,false);
                }
                TextView t1 = convertView.findViewById(android.R.id.text1);
                TextView t2 = convertView.findViewById(android.R.id.text2);
                t1.setText(places.get(position)[0]);
                t2.setText(places.get(position)[1]);
                return convertView;
            }
        };*/
        /*mPlacesListView.setAdapter(placesAdapter);*/

        /*mPlacesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(MainActivity.this,
                        adapterView.getItemAtPosition(i).toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });*/

    }

    public void addPlace(View view)
    {
        EditText name = findViewById(R.id.city_name_et);
        EditText desc = findViewById(R.id.city_desc_et);
        String cn = name.getText().toString();
        String cd = desc.getText().toString();
        places.add(new String[]{cn,cd});
        
        name.setText("");
        desc.setText("");
        Toast.makeText(this, "City Added", Toast.LENGTH_SHORT).show();
    }

    public void showContent(View view)
    {
        MyAdapter myAdapter = new MyAdapter(this,places);
        mPlacesListView.setAdapter(myAdapter);
    }

    class MyAdapter extends BaseAdapter{

        private Context context;
        private List<String[]> places;

        public MyAdapter(Context context, List<String[]> places) {
            this.context = context;
            this.places = places;
        }

        // We have to convey the total number of items we have to display on listview
        @Override
        public int getCount() {
            return places.size();
        }

        @Override
        public String[] getItem(int i) {
            return places.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView==null){
                convertView = LayoutInflater.from(context).inflate(R.layout.one_row_design,parent,false);
            }
            TextView t1 = convertView.findViewById(R.id.city_name);
            TextView t2 = convertView.findViewById(R.id.city_description);
            t1.setText(getItem(position)[0]);
            t2.setText(getItem(position)[1]);
            return convertView;
        }
    }

    private void intializePlaces() {
        places = new ArrayList<String[]>();
        /*places.add(new String[]{"Hyderabad","https://www.google.co.in"});
        places.add(new String[]{"Bengaluru","Silicon Valley of India"});
        places.add(new String[]{"Kochi","part of Gods Own Country"});
        places.add(new String[]{"Chennai","High Humidity"});
        places.add(new String[]{"Vizag","Beautiful Place"});
        places.add(new String[]{"Coimbatore","Tamil Nadu"});
        places.add(new String[]{"Hyderabad","Great City"});
        places.add(new String[]{"Bengaluru","Silicon Valley of India"});
        places.add(new String[]{"Kochi","part of Gods Own Country"});
        places.add(new String[]{"Chennai","High Humidity"});
        places.add(new String[]{"Vizag","Beautiful Place"});
        places.add(new String[]{"Coimbatore","Tamil Nadu"});*/
    }
}