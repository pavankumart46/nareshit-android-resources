package com.nareshit.sqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DatabaseHelper(this);
        db.insertData("PAVAN",12,967849);

        readData();
    }

    private void readData() {
        Cursor c = db.readData();
        if(c!=null){
            c.moveToFirst();
            do{
                Toast.makeText(this, c.getString(1), Toast.LENGTH_SHORT).show();
            }while(c.moveToNext());
        }
    }
}