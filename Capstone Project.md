# Capstone Project - to be submitted before 31-12-2021
###### ( This exercise helps you evaluate your learning yourself  )

##### Step 1: Add Google Signin to your application and authenticate the user (This should happen only once, next time the user opens the app, you need not to authenticate him/her)
![reference](https://bitbucket.org/pavankumart46/nareshit-android-resources/raw/292235307b780df590776bddc671c1bfc63e36a3/Google%20Sign%20In/1.png)
##### Step 2: On the MainActivity.java inflate a menu file with two options (1. US, 2. India, 3. Favorite News)
![refernce](https://bitbucket.org/pavankumart46/nareshit-android-resources/raw/292235307b780df590776bddc671c1bfc63e36a3/Google%20Sign%20In/2.png)
![Menu Reference](https://bitbucket.org/pavankumart46/nareshit-android-resources/raw/292235307b780df590776bddc671c1bfc63e36a3/Google%20Sign%20In/3.png)
##### Step 3: By Deafult you need to fetch the us news and display on the mainactivity's recyclerview.
                - Make sure You get the news Thumbnail, News title and news description
##### Step 4: you create a details screen where you will be displaying the news entirely and also keep a button ( a heart symbol ) to save this news as a favorite news in the database(SQLite)
![Deatils Screen Reference](https://bitbucket.org/pavankumart46/nareshit-android-resources/raw/292235307b780df590776bddc671c1bfc63e36a3/Google%20Sign%20In/4.png)

###### Note: if the user selects favorite, the app should also work for this option in offline.
###### Check if there is internet connection before you make a request for data.