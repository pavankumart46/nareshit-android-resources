package com.nareshit.googlebooks;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nareshit.googlebooks.models.Item;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {

    Context context;
    List<Item> bookData;

    public BookAdapter(Context context, List<Item> bookData) {
        this.context = context;
        this.bookData = bookData;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.one_item_design, parent, false);
        return new BookViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        String title = bookData.get(position).getVolumeInfo().getTitle();
        List<String> a = bookData.get(position).getVolumeInfo().getAuthors();
        String authors = "";
        if(a!=null){
             authors = a.get(0);
            for(int i=1; i<a.size(); i++){
                authors = authors+","+a.get(i);
            }
        }

        if(bookData.get(position).getVolumeInfo().getImageLinks()!=null){
            String img_link = bookData.get(position).getVolumeInfo().getImageLinks().getThumbnail();
            Glide.with(context).load(img_link).into(holder.img);
        }

        holder.book_title.setText(title);
        holder.book_author.setText(authors);

        if(bookData.get(position).getSaleInfo().getSaleability().equals("FOR_SALE")){
            holder.buy_link.setVisibility(View.VISIBLE);
            holder.textView.setVisibility(View.INVISIBLE);
        }else{
            holder.buy_link.setVisibility(View.INVISIBLE);
            holder.textView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return bookData.size();
    }

    class BookViewHolder extends RecyclerView.ViewHolder{

        ImageView img;
        TextView book_title,book_author;
        Button buy_link;
        TextView textView;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.book_thumbnail);
            book_title = itemView.findViewById(R.id.book_title);
            book_author = itemView.findViewById(R.id.book_authors);
            buy_link = itemView.findViewById(R.id.book_purchase);
            textView = itemView.findViewById(R.id.textView);

            buy_link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    String l = bookData.get(pos).getSaleInfo().getBuyLink();
                    Intent i = new Intent(Intent.ACTION_VIEW,Uri.parse(l));
                    context.startActivity(i);
                }
            });
        }

    }
}
