package com.nareshit.intentsexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private TextView mWelcomeMessage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        mWelcomeMessage = findViewById(R.id.welcome_message);
        // Completed: Catch the intent Object and Extract the Extra Values.
        Intent i = getIntent();
        String name = i.getStringExtra("USER_NAME");
        // Completed: Display the data (Extra Value) on the TextView (welcome_message)
        mWelcomeMessage.setText("Welcome, "+name);
    }
}