package com.nareshit.favoritemovies;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Integer> movie_posters;
    private List<String> movie_names;
    private List<String> movie_actors;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intializeData();
        recyclerView = findViewById(R.id.recyclerview);
        MovieAdapter movieAdapter = new MovieAdapter(this,movie_posters,movie_names,movie_actors);
        recyclerView.setAdapter(movieAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        /*recyclerView.setLayoutManager(new GridLayoutManager(this,
                2,RecyclerView.HORIZONTAL,false));*/
        /*recyclerView.setLayoutManager(
                new StaggeredGridLayoutManager(2,
                        StaggeredGridLayoutManager.VERTICAL));*/
    }

    // TODO: Step 1 - Prepare the data
    private void intializeData() {
        movie_posters = new ArrayList<Integer>();
        movie_names = new ArrayList<String>();
        movie_actors = new ArrayList<String>();

        // First Movie
        movie_posters.add(R.drawable.a);
        movie_names.add("Andarivadu");
        movie_actors.add("Chiranjeevi");

        //Second Movie
        movie_posters.add(R.drawable.b);
        movie_names.add("Bombay");
        movie_actors.add("Aravind Swamy");

        // Third Movie
        movie_posters.add(R.drawable.c);
        movie_names.add("Chitram");
        movie_actors.add("Uday Kiran");

        // Fourth Movie
        movie_posters.add(R.drawable.d);
        movie_names.add("Danger");
        movie_actors.add("Naresh");

        // Fifth Movie
        movie_posters.add(R.drawable.e);
        movie_names.add("E EE");
        movie_actors.add("Neiraj Sham");

        // sixth Movie
        movie_posters.add(R.drawable.f);
        movie_names.add("Fast And Furious");
        movie_actors.add("Vin Diesel");

        // Seventh Movie
        movie_posters.add(R.drawable.g);
        movie_names.add("Ganesh");
        movie_actors.add("Venkatesh");

        // Eighth Movie
        movie_posters.add(R.drawable.h);
        movie_names.add("Hello");
        movie_actors.add("Akhil");

        // ninth movie
        movie_posters.add(R.drawable.i);
        movie_names.add("Interstellar");
        movie_actors.add("Matthew McConaughey");

        // Tenth Movie
        movie_posters.add(R.drawable.j);
        movie_names.add("Joker");
        movie_actors.add("Joaquin Phoenix");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.first:
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                break;
            case R.id.second:
                recyclerView.setLayoutManager(new GridLayoutManager(this,2));
                break;
            case R.id.third:
                recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2,
                        StaggeredGridLayoutManager.VERTICAL));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}