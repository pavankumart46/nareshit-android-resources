package com.nareshit.inputcontrols;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private TextView mGenderResult;
    private Spinner mFruitsSpinner;
    private TextView mFruitSelected;
    private Switch aSwitch;
    private ToggleButton toggleButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mGenderResult = findViewById(R.id.gender_result);
        mGenderResult.setText("MALE");
        mFruitsSpinner = findViewById(R.id.fruits);
        mFruitSelected = findViewById(R.id.fruit_selected);
        mFruitSelected.setText("APPLE");
        aSwitch = findViewById(R.id.switch1);
        aSwitch.setChecked(true);
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    Toast.makeText(MainActivity.this, "SWITCH IS ON", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this, "SWITCH IS OFF", Toast.LENGTH_SHORT).show();
                }
            }
        });

        toggleButton = findViewById(R.id.toggleButton);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    Toast.makeText(MainActivity.this, "SWITCH IS ON", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this, "SWITCH IS OFF", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Completed : Create an Adapter (that connects data and views)
        ArrayAdapter<String> fruitsAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.fruits_list));
        // Completed : Set this Adapter on top of the Spinner
        mFruitsSpinner.setAdapter(fruitsAdapter);

        mFruitsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String fruit_name = adapterView.getItemAtPosition(i).toString();
                mFruitSelected.setText(fruit_name);
                mFruitSelected.setAllCaps(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // Need not to implement this right away
            }
        });

    }

    // This method gets invoked as soon as the gender is selected
    public void genderSelection(View view)
    {
        int id = view.getId(); // This gets the selected radiobutton's ID
        switch (id){
            case R.id.male:
                //Do Some Action
                mGenderResult.setText("Male");
                mGenderResult.setAllCaps(true);
                break;
            case R.id.female:
                //Do Some Action
                mGenderResult.setText("female");
                mGenderResult.setAllCaps(true);
                break;
            case R.id.others:
                //Do Some Action
                mGenderResult.setText("others");
                mGenderResult.setAllCaps(true);
                break;
        }
    }

}