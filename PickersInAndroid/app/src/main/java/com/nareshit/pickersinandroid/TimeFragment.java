package com.nareshit.pickersinandroid;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class TimeFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR);
        int min = c.get(Calendar.MINUTE);
        TimePickerDialog picker = new TimePickerDialog(getActivity(),this,hour,min,false);
        return picker;
    }

    // This method will be callled as soon as the user selects the time
    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int min) {
        MainActivity activity = (MainActivity) getActivity();
        String ampm = "AM";
        if(hour>12){
            ampm = "PM";
            hour = hour - 12;
        }
        activity.timeDisplay(hour,min,ampm);
    }
}
