package com.nareshit.scoretracker;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

/** This is documentation comments
 *  Here we write stuff that helps us understand about this class
 *  and also help us in preparing the documentation for the same.
 */

/* The Shortcut to remove unused packages
ctrl + alt + o
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mResult;
    private int mCount = 0;
    public static final String SAVE_INSTANCE_KEY = "COUNT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mResult = findViewById(R.id.result_tv);
//        registerForContextMenu(mResult);
        Button plus_btn = findViewById(R.id.plusBtn);
        Button minus_btn = findViewById(R.id.minusBtn);
        // Completed: Retrieve the activity(previous instance) values back to the activity
        if(savedInstanceState!=null && savedInstanceState.containsKey(SAVE_INSTANCE_KEY)){
           mCount = savedInstanceState.getInt(SAVE_INSTANCE_KEY);
           mResult.setText(String.valueOf(mCount));
        }
        plus_btn.setOnClickListener(this);
        minus_btn.setOnClickListener(this);
        mResult.setOnClickListener(this);

        /*plus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Handle Plus Btn Logic
                mCount++;
                mResult.setText(String.valueOf(mCount));
            }
        });

        minus_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Handle Minus Btn logic
                mCount--;
                mResult.setText(String.valueOf(mCount));
            }
        });*/
    }

    // Completed : To Attach the menu file to the activity override onCreateOptionsMenu()
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Completed : to attach (inflation) layout file with the help of getMenuInflater()
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    // Complete : override onOptionsItemSelected() method to get the menu items into action
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.reset:
                mCount = 0;
                mResult.setText(String.valueOf(mCount));
                break;
            case R.id.settings:
                Toast.makeText(this, "SETTINGS IS SELECTED!", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // Completed: Override onSaveInstanceState Method to save the values
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVE_INSTANCE_KEY,mCount);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.plusBtn){
            mCount++;
        }else if(view.getId() == R.id.minusBtn){
            mCount--;
        }else{
            //Instantiate a PopupMenu with its constructor,which takes the current application
            // Context and the View to which the menu should be anchored.
            PopupMenu pm = new PopupMenu(this,mResult);
            //Use MenuInflater to inflate your menu resource into the Menu object returned by
            // PopupMenu.getMenu().
            pm.getMenuInflater().inflate(R.menu.menu,pm.getMenu());
            //Call PopupMenu.show().
            pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    switch (menuItem.getItemId()){
                        case R.id.reset:
                            mCount = 0;
                            mResult.setText(String.valueOf(mCount));
                            break;
                        case R.id.settings:
                            Toast.makeText(MainActivity.this, "SETTINGS IS SELECTED!", Toast.LENGTH_SHORT).show();
                            break;
                    }
                    return false;
                }
            });
            pm.show();
        }

        mResult.setText(String.valueOf(mCount));
    }

    /*public void scoreIncrease(View view) {
        // when the '+' button is clicked, This method gets called.
        // we can handle the button click (logically) from this method
        // Toast.makeText(this, "Button + is Clicked", Toast.LENGTH_SHORT).show();
        // Completed: First Increment the value
        mCount++;
        // Completed: Show up the Content On the TextView
        mResult.setText(String.valueOf(mCount));
        // Completed : get the current value of the textview in string format
        // String value = mResult.getText().toString();
        // Completed : convert the string value to integer value
        // int v = Integer.parseInt(value);
        // completed : Increment the value
        // v++;
        // Completed : display the value directly on top of the textview
        // mResult.setText(String.valueOf(v));
    }*/


    /*public void scoreDecrease(View view) {
        // when the '-' button is clicked, This method gets called.
        // we can handle the button click (logically) from this method
        // Toast.makeText(this, "Button - is clicked", Toast.LENGTH_SHORT).show();
        // completed : First Decrement the value
        mCount--;
        // Completed : Show up the Content On the TextView
        mResult.setText(String.valueOf(mCount));
    }*/


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu,menu);
        menu.setHeaderTitle("THIS IS CONTEXT MENU");
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.reset:
                mCount = 0;
                mResult.setText(String.valueOf(mCount));
                break;
            case R.id.settings:
                Toast.makeText(this, "SETTINGS IS SELECTED!", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onContextItemSelected(item);
    }
}