# SQLite
``` SQLite is a Database that is desined for handling minimal amount of relational data ```

# Basic Commands of SQLite
### CREATE a table

> CREATE TABLE STUDENTS(stu_id INTEGER PRIMARY KEY AUTOINCREMENT, 
                      student_name TEXT,
                      student_age INTEGER);


### INSERT Values into the Table
> INSERT INTO  
STUDENTS(student_name, student_age) 
VALUES ("Pavan", 28);
					
### SELECT - Read values from the database table

> SELECT * FROM STUDENTS;

#### Use Where Clause to filter students based on age

> SELECT student_name FROM STUDENTS WHERE student_age>22;

#### Use Order by to sort the students based on a cloumn and the order (ASC, DESC)

> SELECT student_name,student_age from STUDENTS where student_age>22 order BY student_name DESC;

#### DROP The TABLE
> DROP TABLE STUDENTS;


#### Delete a Particular row
> DELETE FROM STUDENTS where student_name="Pavan";

#### DELETE all the rows
> DELETE FROM STUDENTS;