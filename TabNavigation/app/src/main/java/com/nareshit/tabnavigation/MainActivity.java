package com.nareshit.tabnavigation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

// TODO 1: Create ViewPager in activity_main.xml
// TODO 2: Create Data - Create three or four fragments with layouts.
// TODO 3: populate this data (fragment screens) on ViewPager
//  with the help of an Adapter
public class MainActivity extends AppCompatActivity {
    private ViewPager vp;
    private TabLayout tl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vp = findViewById(R.id.viewpager);
        tl = findViewById(R.id.tablayout);
        ViewPagerAdapter vpa = new ViewPagerAdapter(getSupportFragmentManager());
        vp.setAdapter(vpa);
        tl.setupWithViewPager(vp);
    }

    // How to override the default functionality of the back button
    /*@Override
    public void onBackPressed() {
        Toast.makeText(this, "You cannot close me by clicking on the back button", Toast.LENGTH_SHORT).show();
    }*/
}
