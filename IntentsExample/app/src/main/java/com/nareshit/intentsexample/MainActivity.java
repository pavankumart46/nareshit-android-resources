package com.nareshit.intentsexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText mInputet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mInputet = findViewById(R.id.input_text);
    }

    // Handling the Button Click
    public void takeMeNext(View view)
    {
        // Completed: Read the Text Entered by the user on the EditText Box as soon as the button is clicked
        String name = mInputet.getText().toString();
        // Logic to open the SecondActivity.class has to be written here
        // Completed: Create an Intent Object (explicit) with source and destination.
        Intent i = new Intent(this,SecondActivity.class);
        // Completed: Attach the Data to the Current intent object using putExtra method.
        i.putExtra("USER_NAME",name);
        // Completed: start the Activity with the help of startActivity()
        startActivity(i);
    }
}