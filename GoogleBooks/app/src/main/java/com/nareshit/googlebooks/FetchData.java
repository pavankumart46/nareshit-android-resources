package com.nareshit.googlebooks;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.nareshit.googlebooks.models.SourceData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.transform.Source;

/**
 * AsyncTask<TYPE1, TYPE2, TYPE3>
 * TYPE 1 -> Input Parameter type for doInBackground() method
 * TYPE 2 -> Input parameter type for onProgressUpdate() method
 * TYPE 3 -> Input Parameter type for onPostExecute() method
 * if no parameter is required, simply use 'Void'
 */

public class FetchData extends AsyncTask<String, Void, String> {

    public static final String LINK = "https://www.googleapis.com/books/v1/volumes?q=";
    Context context;
    ProgressBar progressBar;
    RecyclerView recyclerView;

    public FetchData(Context context, ProgressBar progressBar, RecyclerView recyclerView) {
        this.context = context;
        this.progressBar = progressBar;
        this.recyclerView = recyclerView;
    }

    // This method runs on WorkerThread
    // Write the long running task logic inside this method
    @Override
    protected String doInBackground(String... strings) {
        String query = strings[0];
        try {
            // Convert the string url to URL type object
            URL url = new URL(LINK + query);
            // Open HttpsURLConnection on this URL
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            // get the InputStream
            InputStream is = connection.getInputStream();
            // Get the InputStreamReader ready to read the inputStream
            InputStreamReader isr = new InputStreamReader(is);
            // Create BufferedReader object and pass isr as the constructor argument
            BufferedReader br = new BufferedReader(isr);
            // Read the data line by line and add it to StringBuilder class object
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            // Return String object
            return sb.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // This method runs on Main Thread or UI Thread
    // This method is responsible for Processing the results and
    // publishing them to the UI
    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
        Gson gson = new Gson();
        SourceData sourceData = gson.fromJson(s, SourceData.class);
        /*List<BookData> books = new ArrayList<BookData>();*/
       /* try {
            JSONObject root = new JSONObject(s);
            JSONArray items = root.getJSONArray("items");
            for(int j=0; j<items.length(); j++){
                JSONObject bookInfo = items.getJSONObject(j);
                JSONObject volumeInfo = bookInfo.getJSONObject("volumeInfo");
                String bookname = volumeInfo.getString("title");
                JSONArray authors_array = volumeInfo.getJSONArray("authors");
                String book_authors = authors_array.getString(0);
                for(int i=1; i<authors_array.length(); i++){
                    book_authors = book_authors+","+authors_array.getString(i);
                }
                JSONObject thumbnails_images = volumeInfo.getJSONObject("imageLinks");
                String url_img = thumbnails_images.getString("thumbnail");
                JSONObject saleInfo = bookInfo.getJSONObject("saleInfo");
                String for_sale = saleInfo.getString("saleability");
                BookData  bookData = new BookData(bookname,book_authors,"Buy Link",url_img);
                books.add(bookData);
            }
            // You will have entire data needed by the time for loop executes
            Toast.makeText(context, books.get(0).getBook_name(), Toast.LENGTH_SHORT).show();
            BookAdapter adapter = new BookAdapter(context,books);
            recyclerView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        BookAdapter ba = new BookAdapter(context,sourceData.getItems());
        recyclerView.setAdapter(ba);
    }
}

/**
 * Notes for Parsing JSON Resoponse
 * JSONObject class - this gets the JsonObject from the text, or from the array
 * JSON Object is represented by a pair of code blocks '{}'
 * JSONArray class - this gets the JsonArray from the text, from the jsonObject.
 * Data
 * - String
 * - int
 * - double
 * The data is mapped in key and value pairs. The keys have to be unique and the data can also be null.
 */
