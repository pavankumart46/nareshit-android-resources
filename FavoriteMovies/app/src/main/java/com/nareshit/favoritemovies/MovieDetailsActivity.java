package com.nareshit.favoritemovies;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MovieDetailsActivity extends AppCompatActivity {

    public static final String IMAGE_CONSTANT = "IMG";
    public static final String NAME_CONSTANT = "NAME";
    public static final String ACTOR_CONSTANT = "ACTOR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        ImageView detailsMoviePoster = findViewById(R.id.details_poster);
        TextView detailsMovieName = findViewById(R.id.details_movie_name);
        TextView detailsMovieActor = findViewById(R.id.details_movie_actor);

        // First Catch the intent
        Intent i = getIntent();
        // Extract the Data
        int img = i.getIntExtra(IMAGE_CONSTANT,0);
        String n = i.getStringExtra(NAME_CONSTANT);
        String a = i.getStringExtra(ACTOR_CONSTANT);

        if(img!=0){
            detailsMoviePoster.setImageResource(img);
        }
        detailsMovieName.setText(n);
        detailsMovieActor.setText(a);
    }
}