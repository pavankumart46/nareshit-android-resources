package com.nareshit.foregroundservices;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

// TODO 2: Create a class and extend the same to Service Class and also override the unimplemented methods
public class ForegroundService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // The service is starting, due to a call to startService()
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            NotificationChannel channel =
                    new NotificationChannel("servicechannel","foreground channel", NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"servicechannel");
        builder.setSmallIcon(R.drawable.ic_launcher_foreground);
        builder.setContentTitle("FOREGROUND SERVICE STARTED");
        builder.setContentText("This Notificaition cannot be cancelled until the service is stopped");

        // For Isuign the notification do not use manager.notify() instead use startForeground() method
        startForeground(123, builder.build());

        /*try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
*/
        //stopSelf(); // This method will stop the service automatically

        return super.onStartCommand(intent, flags, startId);
    }

    // OnBind method is used for bound serices, Leave it to return null
    // as we are working with foreground services.
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
