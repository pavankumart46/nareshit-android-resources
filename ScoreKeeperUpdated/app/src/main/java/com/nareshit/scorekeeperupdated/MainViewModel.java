package com.nareshit.scorekeeperupdated;

import android.util.Log;
import android.view.View;

import androidx.lifecycle.ViewModel;

public class MainViewModel extends ViewModel {

    private int count = 0;
    public MainViewModel() {
        // Know when a View Model is created.
        Log.i("MainViewModel", "View Model Created");
    }

    // This is a method that gets encountered just before the view model is
    // Destroyed
    @Override
    protected void onCleared() {
        super.onCleared();
        Log.i("MainViewModel", "View Model Destroyed");
    }

    // The following method increments the score by 1
    public void increment(){
        count++;
    }

    // The following method Decrements the score by 1
    public void decrement(){
        count--;
    }

    // The following method returns the score to the calling function
    public int getCount(){
        return count;
    }
}
