package com.nareshit.commonintents;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;
import static android.provider.Settings.ACTION_WIFI_SETTINGS;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 322;
    private ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.captured_image);
    }

    // This method will handle the button click
    public void openWeb(View view)
    {
        String url_to_open = "https://www.facebook.com";
        // TODO 1: Convert the url to uri object using Uri.parse()
        Uri u = Uri.parse(url_to_open);
        // TODO 2: Create an Intent Object
        Intent i = new Intent();
        // TODO 3: Set the Action type to ACTION_VIEW
        i.setAction(Intent.ACTION_VIEW);
        // TODO 4: Set the DataScheme using intent.setData(pass the uri object)
        i.setData(u);
        // TODO 5: Check if there any available Activities in the android system to process the request
        if(i.resolveActivity(getPackageManager())!=null)
        {
            // TODO 6: if yes from todo5, startActivity
            startActivity(i);
        }
        else {
            Toast.makeText(this, "NO ACTIVITY FOUND", Toast.LENGTH_SHORT).show();
        }

    }

    // This method gets invoked when the open Maps button is clicked.
    public void openMaps(View view)
    {
        EditText et = findViewById(R.id.city_name);
        String city_name = et.getText().toString();
        // TODO 1 : Create an Intent and set the two arguments
            // TODO: setAction()
            // TODO: setData() - Should be in URI format.
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q="+city_name));
        // TODO 2: startActivity()
        startActivity(i);
    }

    public void openWifiSettings(View view)
    {
        Intent i = new Intent(ACTION_WIFI_SETTINGS);
        startActivity(i);
    }


    public void openDailer(View view) {
        String phone = "9848032919";
        Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+phone));
        startActivity(i);
    }


    public void openCamera(View view)
    {
        // Completed : Create an intent with ACTION_IMAGE_CAPTURE as the action type.
        Intent camera_intent = new Intent(ACTION_IMAGE_CAPTURE);
        // Completed : Call startActivityForResult(intent, REQUEST_CODE) - is to get the image captured
        if(camera_intent.resolveActivity(getPackageManager())!=null){
            startActivityForResult(camera_intent, REQUEST_CODE);
        }
    }
    // Completed : implement onActivityResult(REQUEST_CODE, RESULT_CODE, INTENT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent i) {
        super.onActivityResult(requestCode, resultCode, i);
        // Completed : check if the request code matches the REQUEST_CODE that we sent
        if(resultCode == RESULT_OK){
            if(requestCode == REQUEST_CODE) {
                // Completed : get the data from the extras of the intent with "data" as the key
                // Completed : Convert the image format to BITMAP
                Bitmap b = (Bitmap) i.getExtras().get("data");
                // Completed : Set the image on the imageview (IMAGEVIEW reference)
                imageView.setImageBitmap(b);
            }
        }
    }

}