package com.nareshit.tabnavigation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

public class RedFragment extends Fragment {

    private Button toast_btn;
    public RedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_red, container, false);
        toast_btn = v.findViewById(R.id.toastBtn);
        toast_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "THIS IS A TOAST", Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }
}