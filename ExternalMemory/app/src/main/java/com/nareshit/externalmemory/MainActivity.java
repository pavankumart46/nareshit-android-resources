package com.nareshit.externalmemory;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private TextView result;
    File file;
    //TODO 1: Reading the state of the device
    private boolean isAvailable = false; // Do we have External Memory allocation on the device
    private boolean isReadable = false;  // if we have, is it readable
    private boolean isWritable = false; // if we have , is it writable

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.text_here);
        result = findViewById(R.id.result);
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        file = new File(folder, "ExternalFile.txt");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED &&
                (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(MainActivity.this, "PERMISSIONS ARE GRANTED", Toast.LENGTH_SHORT).show();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
            }
        }

        checkState();
    }

    private void checkState() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            isAvailable = isReadable = isWritable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            isAvailable = isReadable = true;
            isWritable = false;
        } else {
            isAvailable = isReadable = isWritable = false;
        }
    }

    public void writeData(View view) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            String c = editText.getText().toString();
            fos.write(c.getBytes());
            fos.close();
            Toast.makeText(this, "Done " + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
            editText.getText().clear();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void readData(View view) {
        try {
            FileInputStream fis = new FileInputStream(file);
            int i = -1;
            StringBuilder sb = new StringBuilder();
            while((i = fis.read())!=-1){
                sb.append((char)i);
            }
            result.setText(sb.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            for (int i = 0; i < permissions.length; i++) {
                switch (permissions[i]) {
                    case Manifest.permission.READ_EXTERNAL_STORAGE:
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Toast.makeText(MainActivity.this, "READ EXTERNAL STORAGE GRANTED", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, "READ EXTERNAL STORAGE DENIED", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Toast.makeText(MainActivity.this, "WRITE EXTERNAL STORAGE GRANTED", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, "WRITE EXTERNAL STORAGE DENIED", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            }
        }
    }
}
