package com.nareshit.contentresolverstudentdetails;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView result;

    // TODO 1: To Access the content provider, we need to have content URI.
    // Inorder to let the android system understand that this is the content provider's
    // URI, prefix it with ""content://""
    String authorities = "content://com.nareshit.studentdetailssql.cp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result = findViewById(R.id.result);

        // TODO 2: Convert the existing content uri (which is in string format),
        //  to Uri type object
        Uri CONTENT_URI = Uri.parse(authorities);

        // TODO 3: Pass the request to the content Provider
        Cursor cursor = getContentResolver().query(CONTENT_URI, null, null,
                null, null);

        // TODO 4: Parse the cursor data and show it on the textview
        if (cursor != null) {
            cursor.moveToFirst();
            StringBuilder sb = new StringBuilder();
            do {
                Toast.makeText(this, cursor.getString(1), Toast.LENGTH_SHORT).show();
                sb.append("ID: " + cursor.getInt(0) + " ");
                sb.append("Student Name: " + cursor.getString(1) + " ");
                sb.append("Student Age: " + cursor.getInt(2) + "\n\n");
            } while (cursor.moveToNext());
            cursor.close();
            result.setText(sb.toString());
        }
    }
}