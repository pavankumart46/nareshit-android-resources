package com.nareshit.sharedpreferences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener
{

    private TextView textView;
    private EditText name,age;
    private SharedPreferences preferences;
    private static final String PREFERENCES_NAME = "com.nareshit.sharedpreferences.SHARED_PREFERENCES";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferences = getSharedPreferences(PREFERENCES_NAME,MODE_PRIVATE);
        textView = findViewById(R.id.textView);
        name = findViewById(R.id.name);
        age = findViewById(R.id.age);
        preferences.registerOnSharedPreferenceChangeListener(this);

    }

    public void saveData(View view) {
        String n = name.getText().toString();
        int a = Integer.parseInt(age.getText().toString());
        // EDITING SHARED PREFERENCES OBJECT
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("NAME",n);
        editor.putInt("AGE",a);
        editor.apply();

        name.getText().clear();
        age.getText().clear();
    }

    public void loadData(View view) {
        textView.setText("");
        // FEtch the data from shared preferences
        if (preferences != null) {
            String n = preferences.getString("NAME","NOT CREATED SO FAR");
            int a = preferences.getInt("AGE",0);

            textView.setText("NAME: "+ n+"\nAge: "+a);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences preferences, String s) {
        textView.setText("");
        // FEtch the data from shared preferences
        if (preferences != null) {
            String n = preferences.getString("NAME","NOT CREATED SO FAR");
            int a = preferences.getInt("AGE",0);

            textView.setText("NAME: "+ n+"\nAge: "+a);
        }
    }
}