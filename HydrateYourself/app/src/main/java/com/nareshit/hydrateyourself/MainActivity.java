package com.nareshit.hydrateyourself;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView totalGlasses;
    private Switch waterRemainder;
    private AlarmManager manager;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

        manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        preferences = getSharedPreferences("MY_PREFERENCES",MODE_PRIVATE);
        if(preferences!=null){
            int count = preferences.getInt("WATER_COUNT",0);
            totalGlasses.setText(String.valueOf(count));
            boolean status = preferences.getBoolean("SWITCH_STATE", false);
            waterRemainder.setChecked(status);
        }
        // The following Pending Intent is created to take us to AlarmReceiver Broadcast Receiver
        PendingIntent pi = PendingIntent.getBroadcast(MainActivity.this,
                1231,
                new Intent(MainActivity.this,AlarmReceiver.class),
                PendingIntent.FLAG_UPDATE_CURRENT);

        // To check if there is an active alarm
        /*boolean alarmStatus = (PendingIntent.getBroadcast(this,
                1231,new Intent(this,AlarmReceiver.class),
                PendingIntent.FLAG_NO_CREATE)!=null);*/

        // set the alarmstatus on the switch.
        /*waterRemainder.setChecked(alarmStatus);*/

        waterRemainder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    // When the switch is on, we have to create alarm
                    int ALARM_TYPE = AlarmManager.ELAPSED_REALTIME;
                    // Elapsed Real Time is calculated from the time since the system booted
                    // - Return Type of the method "elapsedRealtime() is in milliseconds (long)"
                    long triggerTime = SystemClock.elapsedRealtime();
                    // We have to give the repeating interval time also in Milliseconds (long)
                    long intervalTime = 60*1000;

                    // Start the Alarm
                    manager.setInexactRepeating(ALARM_TYPE,triggerTime,intervalTime,pi);
                    Toast.makeText(MainActivity.this, "ALARM IS SET", Toast.LENGTH_SHORT).show();
                    // To Preserve the state of Switch
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("SWITCH_STATE",true);
                    editor.apply();
                }else{
                    manager.cancel(pi);
                    Toast.makeText(MainActivity.this, "ALARM IS OFF", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("SWITCH_STATE",false);
                    editor.apply();
                }
            }
        });
    }



    // This method will be invoked when there is a button click
    public void drinkWater(View view) {
        if(preferences!=null){
            int count = preferences.getInt("WATER_COUNT",0) + 1;
            // Editing the Shared Preferences
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("WATER_COUNT",count);
            editor.apply();

            totalGlasses.setText(String.valueOf(count));
        }
    }

    private void initViews() {
        imageView = findViewById(R.id.water_img);
        totalGlasses = findViewById(R.id.total_glasses);
        waterRemainder = findViewById(R.id.alarm_switch);
    }
}