package com.nareshtechnologies.roomdatabaseexample;

// TODO 3: Create Database Access Object
import android.hardware.lights.LightState;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

/**
 * WE need Database access object to run queries on the database
 * tables and fetch, load, update data*/

@Dao
public interface StudentDAO {
    // Interfaces doesn't have methods with implementation
    // All methods in interfaces are abstract

    @Insert
    void insertData(Student student);

    @Query("Select * from student")
    List<Student> getStudents();
}
