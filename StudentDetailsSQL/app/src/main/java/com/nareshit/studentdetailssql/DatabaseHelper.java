package com.nareshit.studentdetailssql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

// This class is used to create database, create tables
// and we can also write multiple commands that work around the data
// of the tables.
// TODO 1: Create a class and extend it to SQliteOpenHelper
public class DatabaseHelper extends SQLiteOpenHelper {
    // TODO 2: Implement the methods required by SQLiteOpenHelper
    // TODO 3: Define the database name, table names and column names
    public static final String DATABASE_NAME = "naresh.db";
    public static final String TABLE_NAME = "students";
    public static final String COL0 = "student_id";
    public static final String COL1 = "student_name";
    public static final String COL2 = "student_age";
    public static final int DATABASE_VERSION = 1;

    // TODO 4: Implement the constructor
    private Context context;
    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null , DATABASE_VERSION);
        this.context = context;
    }


    /* This method is used to create database tables*/
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_COMMAND = "CREATE TABLE "+TABLE_NAME+"("+COL0+" INTEGER PRIMARY KEY AUTOINCREMENT," +
                ""+COL1+" TEXT,"+COL2+" INTEGER);";
        db.execSQL(CREATE_TABLE_COMMAND);
        Toast.makeText(context, "TABLE CREATED", Toast.LENGTH_SHORT).show();

    }

    /* if there are any upgrades to the existing database, those upgrades code has to
    * go inside this method*/
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE "+TABLE_NAME);
        onCreate(db);
    }

    //The Following method, inserts the data into the table
    public void insertData(ContentValues contentValues){
        // We are getting the writable data base because, we want to write something into it
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME,null,contentValues);
    }

    // The Following method, returns a cursor of values
    public Cursor getValues(){
        // The result of a Select Query is always in a cursor object
        // to execute SELECT Query, we need to use "rawQuery()" method
        // - Get the readable database
        SQLiteDatabase db = this.getReadableDatabase();
        // - Execute rawQuery() method by passing the Select Query
        return db.rawQuery("SELECT * FROM "+TABLE_NAME, null);
    }

}
