package com.nareshit.musicplayer;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class MusicService extends Service {

    // TODO 1: Create a Media Player Instance
    private MediaPlayer player;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO 2: Initialize the player instance with what ever the music you want to play
        player = MediaPlayer.create(this, R.raw.music1);
        // TODO 3: choice - You can set the looping option or you can avoid it
        player.setLooping(false);
        // todo 4: To start playing the music, we can call start() method on top of the player instance.
        player.start();
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // todo 5: To stop playing the music, we can call stop() method on top of the player instance.
        player.stop();
        player.release();
    }
}
