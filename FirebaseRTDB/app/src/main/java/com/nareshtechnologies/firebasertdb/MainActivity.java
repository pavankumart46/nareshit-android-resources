package com.nareshtechnologies.firebasertdb;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    FirebaseDatabase fireDb;
    DatabaseReference dbr;
    EditText n,a;
    RecyclerView recyclerView;
    RVAdapter rvAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        n = findViewById(R.id.student_name);
        a = findViewById(R.id.student_age);
        fireDb = FirebaseDatabase.getInstance();
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        dbr = fireDb.getReference();
        // With the help of push method, the unique key is generated.

    }

    public void pushData(View view) {
        String name = n.getText().toString();
        String age = a.getText().toString();
        Student s = new Student(name, age);

        // Push Data to firebase console
        dbr.push().setValue(s)
                .addOnSuccessListener(success ->{
                    Toast.makeText(MainActivity.this, "Insetion is Successful", Toast.LENGTH_SHORT).show();
                })
                .addOnFailureListener(err -> {
                    Toast.makeText(MainActivity.this, "Insetion is not Successful", Toast.LENGTH_SHORT).show();                });
    }

    public void loadData(View view) {
        rvAdapter = new RVAdapter(this,dbr);
        recyclerView.setAdapter(rvAdapter);
        // Try Loading the data from firebase
        dbr.orderByKey().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<Student> list = new ArrayList<>();
                List<String> keys = new ArrayList<>();
                for(DataSnapshot data: snapshot.getChildren()){
                    //Toast.makeText(MainActivity.this, data.getKey(), Toast.LENGTH_SHORT).show();
                    keys.add(data.getKey());
                    Student s = data.getValue(Student.class);
                    /*Toast.makeText(MainActivity.this, s.getName(), Toast.LENGTH_SHORT).show()*/;
                    list.add(s);
                }
                rvAdapter.setItems(list,keys);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}

// TODO 1: First Create A Project
// TODO 2: Login to https://console.firebase.google.com
// TODO 3: Create a Project to use any of the firebase services
// TODO 4: to run firebase based applications, we need to check if the
//  Google play services are installed on the emulator
// HOW to check: head to settings -> apps & Notifications -> app info
    // if google play services app is found, that means the google play services
    // are enabled.
    // 99% of the time, all real devices will have google play services.
// TODO 5: set up the firebase
    // -> through firebase android project console
// TODO 6: Push Data to firebase
// Required Structure of data in the firebase realtime databases
// Unique Key
    // - Student name
    // - Student age