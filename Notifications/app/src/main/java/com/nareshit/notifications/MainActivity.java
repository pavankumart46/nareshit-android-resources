package com.nareshit.notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

public class MainActivity extends AppCompatActivity {

    private NotificationManager manager;
    private static final String CHANNEL_ID = "NareshItNotifications";
    private static final int NOTIFICATION_ID = 123;
    private static final int PENDINGINTENT_ID = 234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // NotificationManager is a system service, we can initializze
        // an object of kind NotificationManager with the help of getSystemService()
        // Method
        manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    // This method is used to send the Notification
    public void sendNotification(View view) {
        //TODO 1: Create the Notification Using NotificationCompat.Builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        // TODO 1.1: Set the small ICON
        builder.setSmallIcon(R.drawable.umbrella);
        // TODO 1.2: set the title of the notification
        builder.setContentTitle("SAMPLE NOTIFICATION");
        // TODO 1.3: set the content Text
        builder.setContentText("This can be lengthy. This is the actual message to read");
        // TODO 1.4: set cancelable
        builder.setAutoCancel(true);
        // TODO 1.5: setPriority for all the devices that run Nougaht and below
        builder.setPriority(NotificationCompat.PRIORITY_MAX);
        // TODO 1.6: getting the Notification into Action
        /*
         * Upon tapping the notificaiton, some action has to be performed, to do that follow the steps below*/
        //TODO 1.6.1 : Create an Intent - to define the action
        Intent action = new Intent(this, MainActivity.class);
        // TODO 1.6.2: Create a PendingIntent - To wrap the current intent
        PendingIntent pi = PendingIntent.getActivity(this, PENDINGINTENT_ID, action, PendingIntent.FLAG_UPDATE_CURRENT);
        // TODO 1.6.3: add this pending intent to the current notification
        builder.setContentIntent(pi);
        // TODO 1.7: set the action Button using addAction()
        builder.addAction(R.drawable.umbrella,"ACTION",pi);
        builder.addAction(R.drawable.umbrella,"ACTION",pi);
        builder.addAction(R.drawable.umbrella,"ACTION",pi);
        // TODO 1.8: Set the BigPicturestyle using NotificationCompat.BigPictureStyle()
        // TODO 1.8.1 : Change the format of the image to bitmap
        Bitmap picture = BitmapFactory.decodeResource(getResources(),R.drawable.android);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(),R.drawable.umbrella);
        // TODO 1.8.2: Set the image on top of the Notification in BigPictureStyle()
        /*builder.setStyle(new NotificationCompat.BigPictureStyle()
                .bigPicture(picture)
                .bigLargeIcon(largeIcon));*/
        builder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText(getResources().getString(R.string.bigtext)));
        // TODO 2: Create Notification Channel
        // TODO 2.1: Check the Version of the Current Device where our app is running
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // TODO 2.2: Create Notification Channel using NotificationChannel Class
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Naresh Channel", NotificationManager.IMPORTANCE_HIGH);
            // TODO 2.3: you can set the desc of the channel
            channel.setDescription("This is a sample channel");
            // TODO 2.4: Create this channel using NotificationManager object &
            //  createNotificationChannel Method
            manager.createNotificationChannel(channel);
        }
        // TODO 3: Send the Notification using notify()
        manager.notify(NOTIFICATION_ID, builder.build());
        /* Three Components Involved
         *  - NotificationCompat - Is used to build the notification
         *  - NotificationManager - is used to push the notification, cancel it and create a channel
         *  - NotificationChannel - is used to create Notification Channel
         * */
    }

    // This method is used to cancel the Notification
    public void cancelNotification(View view) {
        manager.cancel(NOTIFICATION_ID);
    }
}