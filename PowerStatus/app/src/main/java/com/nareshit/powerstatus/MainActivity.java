package com.nareshit.powerstatus;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView img;
    // This is the unique string with which our custom broadcast can be identified.
    public static final String CUSTOM_BROADCAST = "com.nareshit.powerstatus.CUSTOM_BROADCAST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img = findViewById(R.id.imageView);
        img.setImageResource(R.drawable.airplane_mode_off);
        // Register for AIRPLANE_MODE_CHANGED broadcast
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intentFilter.addAction(Intent.ACTION_POWER_CONNECTED);
        intentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        registerReceiver(new MyPowerReceiver(img),intentFilter);


        LocalBroadcastManager.getInstance(this).registerReceiver(new MyPowerReceiver(img),new IntentFilter(CUSTOM_BROADCAST));

    }

    public void sendBroadcast(View view) {
        // Custom Broadcast
        Intent intent = new Intent();
        intent.setAction(CUSTOM_BROADCAST);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(new MyPowerReceiver(img));
        unregisterReceiver(new MyPowerReceiver(img));
    }
}

/**
 * Normal Broadcast - sendBroadcast()
 * Ordered Broadcast - SendOrderedBroadcast()
 * Local Broadcast - LocalBroadcastManager.getInstance().sendBroadcast()
 * - Local Broadcast is always secured
 * */