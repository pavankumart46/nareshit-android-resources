# 18-10-2021
## Agenda - Concepts Covered on the day!
#### Layouts, Views and Viewgroups
> *View - What ever the UI Components you see on an Android App is called a VIEW. EX: Buttons, EditText, Slider, RadioButton, etc.,
> *ViewGroup - A ViewGroup is a View that can accommodate other Views and ViewGroups inside it. 
> *Layouts - Layouts gives us the structure to organize the Views of our Screen - all Layouts are ViewGroups. The Root view is always a ViewGroup.
#### [Layout Rules - Click Here](https://bitbucket.org/pavankumart46/nareshit-android-resources/raw/eee1e2aea74f0064ed66f4b51b6b360a101fc6cb/ScoreTracker/app/src/main/res/values/XMLRules.xml/)
#### [Changing the existing layout to Linear Layout of Vertical Orientation - Click Here](ScoreTracker/app/src/main/res/layout/activity_main.xml)

# 19-10-2021
## Agenda - Concepts covered on the day!
#### Desiging the user interface
1. We use SP as the units for changing the textSize - SP - Scalable Pixels.
2. We use DP as the units for setting the dimensions for thee Views - DP- Display Pixels.
[Layout Design Code](ScoreTracker/app/src/main/res/layout/activity_main.xml)


[Handling Button Clicks - Code](ScoreTracker/app/src/main/java/com/nareshit/scoretracker/MainActivity.java)

# 20-10-2021
## Agenda - Concepts Covered on the day!
#### Connecting the textview to the java file (object reference - findviewbyId() method)
#### How to set the text on the TextView using settext() method ?
#### How to Read the documentation related to [TextView](https://developer.android.com/reference/android/widget/TextView)
#### How to Check error Log using logCat?
#### Alternatives to Button's onClick attribute. (SetOnClickListner() method and using OnClickListener() Interface)
[activity_main.xml](ScoreTracker/app/src/main/res/layout/activity_main.xml)
[MainActivity.java](ScoreTracker/app/src/main/java/com/nareshit/scoretracker/MainActivity.java)


# 21-10-2021
## Agenda - Concepts Covered on the day!
#### What are activities?
#### How to create an activity?
#### Intents
#### Types of Intents
	-	 Explicit Intents
	- 	 Implicit Intents
#### Worked with Explicit Intents Example [Click Here for the example](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/IntentsExample/)
#### [For Presentation that is used to explain intents and activities Click Here](https://docs.google.com/presentation/d/1kjxsI9brdVRIx3rqoB0H-1-PmVlzJbiQNf4PyqzZKJM/edit#slide=id.g181baec304_0_0)

# 23-10-2021
# 25-10-2021
## Agenda - Concepts Covered on the day!
#### [Common Intents](https://developer.android.com/guide/components/intents-common)
#### [APP LINK](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/CommonIntents/)
	- Accessing Web Browser
	- Opening Maps
	- Opening Wifi Settings
	- Using Camera on the device
### Assignment 1: Use Set Alarm Common Intent to set an alarm for a specific time (Common Intent Documentation may be used - If possible complete the assignment by 26-10-2021)

# 26-10-2021
# 27-10-2021
## Agenda - Concepts Covered on the day !
#### [Activity LifeCycle](https://developer.android.com/guide/components/activities/activity-lifecycle)
##### [Example  - Explained](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/ActivityLifecycle/)
#### [Saving the Instance State of an Activity](https://developer.android.com/topic/libraries/architecture/saving-states)
##### [Example - Explained](ScoreTracker/app/src/main/java/com/nareshit/scoretracker)
#### [ScrollView in Android](https://developer.android.com/reference/android/widget/ScrollView)
##### [Example - Explained](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/ScrollView/)

# 28-10-2021
## Agenda - Concepts Covered On the Day!
#### [Managing the String Resources](ScoreTracker/app/src/main/res/values/strings.xml)

# 29-10-2021
## Agenda - Concepts Covered on the day!
#### [RadioButtons](https://developer.android.com/guide/topics/ui/controls/radiobutton)
#### [Spinner](https://developer.android.com/guide/topics/ui/controls/spinner)
#### [Example - Input Controls](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/InputControls/app/src/main/)

# 30-10-2021
## Agenda - Concepts Covered on the day!
#### [Menus in Android](https://developer.android.com/guide/topics/ui/menus)
#### How to create Menu XML file ?
	- rightclick on RES > new >Android Resource file> 
	- in the pop up window> give resource file name and then choose menu as the value type.
	- Menu tag will be available by default, you can add items as per your requirement.
#### Options Menu
#### Context Menu
#### Popup Menu
#### Example - Score Tracker Application

# 01-11-2021
# 02-11-2021
## Agenda - Concepts Covered On the day!
#### [Constraint Layout](https://developer.android.com/training/constraint-layout)
#### [Example APP](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/ConstraintLayout/)
#### [Designing the layout for landscape mode](ConstraintLayout/app/src/main/res/layout-land/activity_main.xml)

# 03-11-2021
# 05-11-2021
## Agenda - Concepts Covered on the Day!
#### [List View](https://developer.android.com/reference/android/widget/ListView)
#### [Implementing the ListView - Example](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/PlacesInIndia/app/src/main/)
#### [Customizing the ListView - Example](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/PlacesInIndia/app/src/main/)

# 06-11-2021
# 08-11-2021
# 09-11-2021
## Agenda - Concepts Covered on the Day!
#### [RecyclerView - PRESENTATION](https://docs.google.com/presentation/d/1nFJqH0OSSZmjaycRzEGE6vvsm6jlxghQyoO15KKbkwc/edit?usp=sharing)
#### [RecyclerView - Official Documentation](https://developer.android.com/guide/topics/ui/layout/recyclerview?gclid=Cj0KCQiAsqOMBhDFARIsAFBTN3cRVPR8cRokxjBGYPfKRY5u03SyTHB2Xj_tutmT6EaYENc9_TgsCKgaAmQ8EALw_wcB&gclsrc=aw.ds)
#### [RecyclerView - Example Application](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/FavoriteMovies/)
#### [Compound Buttons - Switch, ToggleButton](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/InputControls/app/src/main/)

# 10-11-2021
# 11-11-2021
# 12-11-2021
# 13-11-2021
# 15-11-2021
# Agenda Concepts Covered on the Day!
#### [Pickers in Android](https://developer.android.com/guide/topics/ui/controls/pickers#:~:text=Android%20provides%20controls%20for%20the,month%2C%20day%2C%20year).)
#### [Tab Navigation](https://developer.android.com/guide/navigation/navigation-swipe-view)
#### [AsyncTask](https://developer.android.com/reference/android/os/AsyncTask)
#### [Google Books - Example App](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/GoogleBooks/)

# 16-11-2021
# 17-11-2021
# Agenda Of the Day - Concepts covered!
#### Async Task and Glide Library
#### [Async Task and Recyclerview](https://developer.android.com/reference/android/os/AsyncTask)

# 18-11-2021
# Agenda Concepts Covered on the Day!
#### [Async Task - GSON library](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/GoogleBooks/)

# 19-11-2021
# 20-11-2021
# 22-11-2021
# 23-11-2021
# 24-11-2021
# Agenda Concepts Covered on the Day!
#### [Boradcast Receivers](https://developer.android.com/guide/components/broadcasts)
#### [POWER STATUS APP - Example for broadcast receivers](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/PowerStatus/)
#### [Notifications In Android](https://developer.android.com/guide/topics/ui/notifiers/notifications)
#### [Example - Notifications](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/Notifications/)

# 25-11-2021
# 26-11-2021
# 27-11-2021
# 29-11-2021
# Agenda Concepts covered on the day!
#### [Alarm Managers](https://developer.android.com/reference/android/app/AlarmManager)
#### [Alarm Manager - Example1](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/HydrateYourself/)
#### [Shared Preferences](https://developer.android.com/training/data-storage/shared-preferences)
#### [Shared Preferences- Example 1](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/HydrateYourself/)
#### [Shared Preferences - Example 2](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/SharedPreferences/)
#### [JobSchedulers](https://developer.android.com/reference/android/app/job/JobScheduler)
#### [JobScheuler - Example](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/JobScheduler/)

# 30-11-2021
# Agenda Concepts Covered on the day!
#### [Permissions](https://developer.android.com/guide/topics/permissions/overview)
#### [Permissions Example](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/PermissionsinAndroid/)

# 03-12-2021
# Agenda Concepts covered on the day!
#### [Services in Android](https://developer.android.com/guide/components/services)
#### [Foreground Service Example](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/ForegroundServices/)

# 04-12-2021
# Agenda Concepts Covered on the day!
#### [Media Player](https://developer.android.com/reference/android/media/MediaPlayer)
#### [Background Service - Example](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/MusicPlayer/)

# 06-12-2021
# 07-12-2021
# 08-12-2021
# 09-12-2021
# 10-12-2021
# 11-12-2021
# Agenda Concepts Covered in the week!
#### [Internal Memory in Android](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/InternalMemory/)
#### [Exterrnal Memory in android](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/ExternalMemory/)
#### [SQlite Database in android](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/StudentDetailsSQL/)
#### [Content Provider - Content Resolver - Accessing Contacts Example](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/MyContacts/)

# 13-12-2021
# Agenda Concepts Covered in the day!
#### [Creating a Conent Provider](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/StudentDetailsSQL/)
#### [Accessing the data of a content provider](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/ContentResolverstudentdetails/)

# 14-12-2021
# Agenda Concepts Covered
#### [Data Binding Library](https://developer.android.com/topic/libraries/data-binding)
#### [Example](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/ScoreKeeperUpdated/)

# 20-12-2021
# Agenda Concepts Covered!
#### [View model](https://developer.android.com/topic/libraries/architecture/viewmodel?gclid=Cj0KCQiAzfuNBhCGARIsAD1nu--B6TIZHjrlm6sYcYPe49uE2qdfC30AaNTVZFrd3IwfnmKwp9It8JMaAqnQEALw_wcB&gclsrc=aw.ds#codelabs)
#### [View Model Example](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/ScoreKeeperUpdated/)

# 21-12-2021
# Agenda Concepts Covered!
#### [Room Database](https://developer.android.com/jetpack/androidx/releases/room?gclid=Cj0KCQiAk4aOBhCTARIsAFWFP9HNxj8t6q9dpezcYE4BEn8VIfH4mG3G9zg-BdOUrFRSsVqioqVV0DoaAotbEALw_wcB&gclsrc=aw.ds)
#### [Room Database Example](https://bitbucket.org/pavankumart46/nareshit-android-resources/src/master/RoomDatabaseExample/)