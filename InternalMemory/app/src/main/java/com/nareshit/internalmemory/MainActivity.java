package com.nareshit.internalmemory;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private TextView result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.text_here);
        result = findViewById(R.id.result);
    }

    /** Write data to the file and save it (Internal Memory)
    * openFileOutput() can be used to open a file in write mode
    * arg1 - file name
    * arg2 - Mode
    * Mode 1: PRIVATE_MODE - the contents will be re written
    * Mode 2: APPEND_MODE - the contents will be appended to the existing content
    * This method returns a FileOutPutStream Object.
    * After opening the file we may use write() method and write content */
    public void writeData(View view) {
        try {
            FileOutputStream fos = openFileOutput("MyFile.txt", MODE_APPEND);
            String r = editText.getText().toString();
            editText.getText().clear();
            fos.write(r.getBytes());
            fos.flush();
            fos.close();
            Toast.makeText(MainActivity.this, "DATA WRITTEN SUCCESSFULLY", Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /** Read the data from the file and save it (Internal Memory)
     * openFileInput()
     * -arg - filename
     * returns a FileInputStream Object*/
    public void readData(View view) {
        try {
            FileInputStream fis = openFileInput("MyFile.txt");
            int a;
            StringBuilder temp = new StringBuilder();
            while ((a = fis.read()) != -1) {
                temp.append((char) a);
            }
            result.setText(temp.toString());
            fis.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        Toast.makeText(MainActivity.this, "DATA READ SUCCESSFULLY", Toast.LENGTH_SHORT).show();
    }
}