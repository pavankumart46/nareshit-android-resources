package com.nareshtechnologies.covid19updates;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class MainActivity extends AppCompatActivity {

    private TextView result;
    private ProgressBar progressBar;

    public static final String URL =
            "https://data.covid19india.org/state_district_wise.json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        result = findViewById(R.id.result);
        progressBar = findViewById(R.id.progressBar);

        /**
         * There are 4 arguments to the StringRequest constructor
         * -> Request Type
         * -> URL
         * -> Response Listener
         * -> Error Listener
         * */
        StringRequest request = new StringRequest(
                // Request TYPE
                Request.Method.GET,
                // The Actual URL
                URL,
                // Response Handler
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        result.setText(response);
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                },
                // Error Handler
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        result.setText(error.getMessage());
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }
        );

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);


    }
}