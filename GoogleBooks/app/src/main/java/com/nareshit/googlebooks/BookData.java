package com.nareshit.googlebooks;

// This is Called as POJO (Plain Old Java Object) class
// This class is the model class for our data.
// Model class data can be fetched using getters.

public class BookData {

    // The data items that we need are
    String book_name;
    String authors_name;
    String buy_link;
    String thumbnail_img;

    public BookData(String book_name, String authors_name, String buy_link, String thumbnail_img) {
        this.book_name = book_name;
        this.authors_name = authors_name;
        this.buy_link = buy_link;
        this.thumbnail_img = thumbnail_img;
    }

    // Create getters to get the data out of the model class

    public String getBook_name() {
        return book_name;
    }

    public String getAuthors_name() {
        return authors_name;
    }

    public String getBuy_link() {
        return buy_link;
    }

    public String getThumbnail_img() {
        return thumbnail_img;
    }
}
