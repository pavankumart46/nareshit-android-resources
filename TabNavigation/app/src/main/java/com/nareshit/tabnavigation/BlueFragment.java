package com.nareshit.tabnavigation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class BlueFragment extends Fragment {

    private EditText name_et;
    private Button name_btn;
    private TextView name_tv;
    public BlueFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_blue, container, false);
        name_et = v.findViewById(R.id.name);
        name_btn = v.findViewById(R.id.showName);
        name_tv = v.findViewById(R.id.name_tv);
        name_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String n = name_et.getText().toString();
                name_tv.setText(n);
            }
        });
        return v;
    }
}