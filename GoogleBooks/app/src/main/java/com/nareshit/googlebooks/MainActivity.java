package com.nareshit.googlebooks;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nareshit.googlebooks.models.SourceData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private EditText searchQuery;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.books_display);
        searchQuery = findViewById(R.id.search_query);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(this,
                        DividerItemDecoration.VERTICAL));
    }

    public boolean isNetworkConnected(){
        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        
    }
    // When the image button is clicked, this method is invoked
    public void SearchQuery(View view) {
        getData();
    }

    public void getData(){
        if(isNetworkConnected()){
            progressBar.setVisibility(View.VISIBLE);
            String query = searchQuery.getText().toString();
            /*new FetchData(this, progressBar,recyclerView).execute(query);*/
            // Initialize the Retrofit Library
            Retrofit r = new Retrofit.Builder()
                    .baseUrl("https://www.googleapis.com/books/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            // Create an instance of BookService and create it
            BookService service = r.create(BookService.class);
            // GET the data
            Call<SourceData> bd = service.getBookData(query);
            bd.enqueue(new Callback<SourceData>() {
                @Override
                public void onResponse(Call<SourceData> call, Response<SourceData> response) {
                    progressBar.setVisibility(View.GONE);
                    SourceData sourceData = response.body();
                    recyclerView.setAdapter(new BookAdapter(MainActivity.this, sourceData.getItems()));
                }

                @Override
                public void onFailure(Call<SourceData> call, Throwable t) {
                    Toast.makeText(MainActivity.this, "UNABLE TO FETCH DATA", Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            displayAlert();
        }
    }

    // This method shows a alert dialog box
    private void displayAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("INTERNET CONNECTION IS DOWN");
        alert.setMessage("Check your mobile data connection or wifi connection");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                     dialogInterface.dismiss();
            }
        });
        alert.setNegativeButton("RETRY", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getData();
            }
        });
        alert.show();
    }
}


// TODO 1: Add the permission for using the internet in AndroidManifest.xml
// <uses-permission android:name="INTERNET"/>
// TODO 2: Fetching Data
// TODO 3: Display it on top of the TextView
