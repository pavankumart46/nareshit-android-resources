package com.nareshtechnologies.roomdatabaseexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText studentName, studentAge;
    private TextView result;
    private StudentDatabase studentDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        // TODO 5: Creating the database
        // allowMainThreadQueries() will allow us to run complicated queries on
        // main thread or UI thread.
        studentDatabase = Room.databaseBuilder(this,
                StudentDatabase.class, "myroom.db")
                .allowMainThreadQueries()
                .build();

    }

    private void initViews() {
        studentName = findViewById(R.id.stu_name);
        studentAge = findViewById(R.id.stu_age);
        result = findViewById(R.id.result);
    }

    // TODO 6: Save the data
    public void saveData(View view) {
        String sn = studentName.getText().toString();
        int sa = Integer.parseInt(studentAge.getText().toString());
        Student s = new Student(sn,sa);
        studentDatabase.myStudentDao().insertData(s);
        Toast.makeText(this, "INSERTED DATA", Toast.LENGTH_SHORT).show();
    }

    public void loadData(View view) {
        List<Student> students= studentDatabase.myStudentDao().getStudents();
        result.setText("");
        for(int i=0; i< students.size(); i++){
            result.append(students.get(i).getStu_id()+"\n"+
                    students.get(i).getStu_name()+"\n"+
                    students.get(i).getStu_age()+"\n\n\n");
        }
    }

}